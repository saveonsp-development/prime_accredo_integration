
/****** Object:  Table [accredo].[Enrollments]    Script Date: 8/11/2023 4:58:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE  IF  EXISTS [accredo].[Enrollments]
GO
CREATE TABLE [accredo].[Enrollments](
	[Id] [nchar](18) NULL,
	[First Name] [nvarchar](40) NULL,
	[Last Name] [nvarchar](80) NULL,
	[Suffix] [nvarchar](40) NULL,
	[Birthdate] [nchar](8) NULL,
	[Drug] [nvarchar](80) NULL,
	[Member ID] [nvarchar](20) NULL,
	[Tertiary ID] [nvarchar](25) NULL,
	[Copay Enrollment Date] [nchar](8) NULL,
	[Copay Bin] [nvarchar](10) NULL,
	[Copay ID] [nvarchar](25) NULL,
	[Group ID] [nvarchar](25) NULL,
	[Card Type] [nvarchar](255) NULL,
	[Credit Card Number] [nvarchar](20) NULL,
	[Security Code] [nvarchar](4) NULL,
	[Expiration Date] [nchar](4) NULL,
	[Client Name] [nvarchar](255) NULL,
	[Client Go Live] [nchar](8) NULL,
	[Status_Changed_to-Enrolled] [nchar](8) NULL,
	[Status_Changed_to_Enrollment_Complete] [nchar](8) NULL,
	[Date Enrollment Complete] [nchar](8) NULL,
	[Date Sent to Accredo] [nchar](8) NULL
) ON [PRIMARY]
GO

DROP TABLE  IF  EXISTS [accredo].[Enrollments_Stage]
GO
CREATE TABLE [accredo].[Enrollments_Stage](
	[Id] [nchar](18) NULL,
	[First Name] [nvarchar](40) NULL,
	[Last Name] [nvarchar](80) NULL,
	[Suffix] [nvarchar](40) NULL,
	[Birthdate] [nchar](8) NULL,
	[Drug] [nvarchar](80) NULL,
	[Member ID] [nvarchar](20) NULL,
	[Tertiary ID] [nvarchar](25) NULL,
	[Copay Enrollment Date] [nchar](8) NULL,
	[Copay Bin] [nvarchar](10) NULL,
	[Copay ID] [nvarchar](25) NULL,
	[Group ID] [nvarchar](25) NULL,
	[Card Type] [nvarchar](255) NULL,
	[Credit Card Number] [nvarchar](20) NULL,
	[Security Code] [nvarchar](4) NULL,
	[Expiration Date] [nchar](4) NULL,
	[Client Name] [nvarchar](255) NULL,
	[Client Go Live] [nchar](8) NULL,
	[Status_Changed_to-Enrolled] [nchar](8) NULL,
	[Status_Changed_to_Enrollment_Complete] [nchar](8) NULL,
	[Date Enrollment Complete] [nchar](8) NULL
) ON [PRIMARY]
GO

DROP TABLE  IF  EXISTS  [accredo].[Declined_Enrollments]
GO
CREATE TABLE [accredo].[Declined_Enrollments](
	[Id] [nchar](18) NULL,
	[First Name] [nvarchar](40) NULL,
	[Last Name] [nvarchar](80) NULL,
	[Suffix] [nvarchar](40) NULL,
	[Birthdate] [nchar](8) NULL,
	[Drug] [nvarchar](80) NULL,
	[Member ID] [nvarchar](20) NULL,
	[Client Name] [nvarchar](255) NULL,
	[Client Go Live] [nchar](8) NULL,
	[Date Sent to Accredo] [nchar](8) NULL
) ON [PRIMARY]
GO

DROP TABLE  IF  EXISTS  [accredo].[Declined_Enrollments_Stage]
GO
CREATE TABLE [accredo].[Declined_Enrollments_Stage](
	[Id] [nchar](18) NULL,
	[First Name] [nvarchar](40) NULL,
	[Last Name] [nvarchar](80) NULL,
	[Suffix] [nvarchar](40) NULL,
	[Birthdate] [nchar](8) NULL,
	[Drug] [nvarchar](80) NULL,
	[Member ID] [nvarchar](20) NULL,
	[Client Name] [nvarchar](255) NULL,
	[Client Go Live] [nchar](8) NULL
) ON [PRIMARY]
GO


DROP TABLE  IF  EXISTS   [accredo].[Unenrollments]
GO
CREATE TABLE [accredo].[Unenrollments](
	[Id] [nchar](18) NULL,
	[First Name] [nvarchar](40) NULL,
	[Last Name] [nvarchar](80) NULL,
	[Suffix] [nvarchar](40) NULL,
	[Birthdate] [nchar](8) NULL,
	[Drug] [nvarchar](80) NULL,
	[Member ID] [nvarchar](20) NULL,
	[Tertiary ID] [nvarchar](25) NULL,
	[Status_Changed_to_Unenrollment_Complete] [nchar](8) NULL,
	[Date Sent to Accredo] [nchar](8) NULL
) ON [PRIMARY]
GO

DROP TABLE  IF  EXISTS   [accredo].[Unenrollments_Stage]
 GO
CREATE TABLE [accredo].[Unenrollments_Stage](
	[Id] [nchar](18) NULL,
	[First Name] [nvarchar](40) NULL,
	[Last Name] [nvarchar](80) NULL,
	[Suffix] [nvarchar](40) NULL,
	[Birthdate] [nchar](8) NULL,
	[Drug] [nvarchar](80) NULL,
	[Member ID] [nvarchar](20) NULL,
	[Tertiary ID] [nvarchar](25) NULL,
	[Status_Changed_to_Unenrollment_Complete] [nchar](8) NULL
) ON [PRIMARY]
GO


