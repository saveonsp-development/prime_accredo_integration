
/****** Object:  StoredProcedure [accredo].[Stage_Enrollment_Data]    Script Date: 8/14/2023 9:58:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


DROP PROCEDURE  IF  EXISTS [accredo].[Stage_Enrollment_Data]
GO
-- =============================================
-- Author:		Tanessa Richardson
-- Create date: 12/28/2021
-- Description:	Stages new enrollments for inclusion on Accredo Enrollment file
-- =============================================
-- TYR 20220107:	JMM and I reviewed the full set of business requirements (including all new logic introduced since 12/29/2021)
--					The query has been updated to reflect the full set of business requirements as they stand as of today
-- TYR 20220113:	DTE-264 Added case statements for [Status_Changed_to_Enrollment_Complete] and [Date Enrollment Complete] output values
--					to account for newly defined business logic whereby we have to send the member eligibility date in these fields if the
--					patient status is Identified Ineligible, to prevent code breakage on the Accredo side
-- TYR 20220315:	DTE-345 Removed temporary logic in place due to non-happy path HCSC clients and added "normal" or "usual" business logic to look
--					at changes from the previous business day
-- TYR 20220816:	DTE-567 Modified logic for enrollments based on patient status and prescribed drug status to no longer be dependent upon
--					changes made the prior business day.  Now picking up any enrollment where the data for the member and prescribed drug don't
--					match the most recent data sent to Accredo.  Added new Patient Status values 'Mbr Requests No Calls', 'Pending CPA Mbr', and
--					'Pending CPA Pharm' to filter criteria
-- JMM 02/14/2023 SFT-2986:  Adjust script to account for Drug__c.Drug_Level_Exception_Pharmacy__c changing to Drug__c.Drug_Level_Outside_Pharmacy__c
-- =============================================
CREATE PROCEDURE [accredo].[Stage_Enrollment_Data]

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @prior_business_day DATETIME
	SET @prior_business_day = (SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
									WHEN 1 THEN -2 
									WHEN 2 THEN -3 
									ELSE -1 
									END, DATEDIFF(DAY, 0, GETDATE()))) 


	/*****************************************************/
	/* Truncate the staging table */
	/*****************************************************/
	TRUNCATE TABLE accredo.Enrollments_Stage

	/*****************************************************
	 Get the most current record from accredo.Enrollments
	 for the member and prescribed drug, making all strings
	 case insensitive
	******************************************************/
	SELECT [Id]
		,[First Name] COLLATE SQL_Latin1_General_CP1_CI_AS AS [First Name]
		,[Last Name] COLLATE SQL_Latin1_General_CP1_CI_AS as [Last Name]
		,Suffix COLLATE SQL_Latin1_General_CP1_CI_AS as Suffix 
		,Birthdate 
		,[Drug] COLLATE SQL_Latin1_General_CP1_CI_AS as [Drug]
		,[Member ID] COLLATE SQL_Latin1_General_CP1_CI_AS as [Member ID] 
		,[Tertiary ID] COLLATE SQL_Latin1_General_CP1_CI_AS as [Tertiary ID] 
		,[Copay Enrollment Date]
		,[Copay Bin] COLLATE SQL_Latin1_General_CP1_CI_AS as [Copay Bin] 
		,[Copay ID] COLLATE SQL_Latin1_General_CP1_CI_AS as [Copay ID] 
		,[Group ID] COLLATE SQL_Latin1_General_CP1_CI_AS as [Group ID] 
		,[Card Type] COLLATE SQL_Latin1_General_CP1_CI_AS as [Card Type] 
		,[Credit Card Number] 
		,[Security Code] 
		,[Expiration Date]
		,[Client Name] COLLATE SQL_Latin1_General_CP1_CI_AS as [Client Name] 
		,[Client Go Live]
		,[Status_Changed_to-Enrolled]
		,Status_Changed_to_Enrollment_Complete
		,[Date Enrollment Complete]
	INTO #most_current_records
	FROM
	(
		SELECT [Id]
			,[First Name]
			,[Last Name]
			,Suffix
			,Birthdate
			,[Drug]
			,[Member ID]
			,[Tertiary ID]
			,[Copay Enrollment Date]
			,[Copay Bin]
			,[Copay ID]
			,[Group ID]
			,[Card Type]
			,[Credit Card Number]
			,[Security Code]
			,[Expiration Date]
			,[Client Name]
			,[Client Go Live]
			,[Status_Changed_to-Enrolled]
			,Status_Changed_to_Enrollment_Complete
			,[Date Enrollment Complete]
			,ROW_NUMBER() OVER ( PARTITION BY [Id], [Drug] ORDER BY [Date Sent to Accredo] DESC) AS row_num
		  FROM [accredo].[Enrollments]
	) x
	WHERE row_num = 1


	/*****************************************************/
	/* Populate staging table */
	/*****************************************************/
	INSERT INTO accredo.Enrollments_Stage
	(
		[Id],
		[First Name],
		[Last Name],
		Suffix,
		[Birthdate],
		[Drug],
		[Member ID],
		[Tertiary ID],
		[Copay Enrollment Date],
		[Copay Bin],
		[Copay ID],
		[Group ID],
		[Card Type],
		[Credit Card Number],
		[Security Code],
		[Expiration Date],
		[Client Name],
		[Client Go Live],
		[Status_Changed_to-Enrolled],
		[Status_Changed_to_Enrollment_Complete],
		[Date Enrollment Complete]
	)
	SELECT [Id]
		,[First Name]
		,[Last Name]
		,Suffix
		,Birthdate 
		,[Drug]
		,[Member ID]
		,[Tertiary ID]
		,[Copay Enrollment Date]
		,[Copay Bin]
		,[Copay ID]
		,[Group ID]
		,[Card Type]
		,[Credit Card Number] 
		,[Security Code] 
		,[Expiration Date]
		,[Client Name]
		,[Client Go Live]
		,[Status_Changed_to-Enrolled]
		,Status_Changed_to_Enrollment_Complete
		,[Date Enrollment Complete]
	FROM
	(
		SELECT [Id]
			,[First Name]
			,[Last Name]
			,Suffix
			,Birthdate 
			,[Drug]
			,[Member ID]
			,[Tertiary ID]
			,[Copay Enrollment Date]
			,[Copay Bin]
			,[Copay ID]
			,[Group ID]
			,[Card Type]
			,[Credit Card Number] 
			,[Security Code] 
			,[Expiration Date]
			,[Client Name]
			,[Client Go Live]
			,[Status_Changed_to-Enrolled]
			,Status_Changed_to_Enrollment_Complete
			,[Date Enrollment Complete]
		FROM
		(
			/***********************************************************************************
			 Retrieve new enrollments based on Contact Patient Status and Prescribed Drug Status
			 ***********************************************************************************/
			SELECT DISTINCT cont.Id
				, cont.FirstName COLLATE SQL_Latin1_General_CP1_CI_AS AS [First Name]
				, cont.LastName COLLATE SQL_Latin1_General_CP1_CI_AS AS [Last Name]
				, cont.Suffix COLLATE SQL_Latin1_General_CP1_CI_AS AS Suffix
				, FORMAT(cont.Birthdate, 'yyyyMMdd') AS [Birthdate]
				, drug.[Name] COLLATE SQL_Latin1_General_CP1_CI_AS AS [Drug]
				, cont.Member_ID__c COLLATE SQL_Latin1_General_CP1_CI_AS AS [Member ID]
				, cont.Tertiary_ID__c COLLATE SQL_Latin1_General_CP1_CI_AS AS [Tertiary ID]
				, FORMAT(pres.Copay_Enrollment_Date__c, 'yyyyMMdd') AS [Copay Enrollment Date]
				, pres.Copay_Bin__c COLLATE SQL_Latin1_General_CP1_CI_AS AS [Copay Bin]
				, pres.Copay_ID__c COLLATE SQL_Latin1_General_CP1_CI_AS AS  [Copay ID]
				, pres.Group_ID__c COLLATE SQL_Latin1_General_CP1_CI_AS AS [Group ID]
				, pres.Card_Type__c COLLATE SQL_Latin1_General_CP1_CI_AS AS [Card Type]
				, pres.Card_Number__c AS [Credit Card Number]
				, pres.Card_Security_Code__c AS [Security Code]
				, FORMAT(CAST(Card_Expiration_Year__c AS DATE), 'yy') + Card_Expiration_Month__c AS [Expiration Date]
				, acct.[Name] COLLATE SQL_Latin1_General_CP1_CI_AS AS [Client Name]
				, FORMAT(cont.Client_Go_Live_Date__c, 'yyyyMMdd') AS [Client Go Live]
				, FORMAT(pres.[Status_Changed_to_Enrolled__c], 'yyyyMMdd') AS [Status_Changed_to-Enrolled]
				, CASE WHEN cont.Patient_Status__c = 'Identified Ineligible' THEN FORMAT(cont.Member_Eligibility_Effective_Date__c, 'yyyyMMdd')
					ELSE FORMAT(cont.Status_Changed_to_Enrollment_Complete__c, 'yyyyMMdd')
					END AS [Status_Changed_to_Enrollment_Complete]
				, CASE WHEN cont.Patient_Status__c = 'Identified Ineligible' THEN FORMAT(cont.Member_Eligibility_Effective_Date__c, 'yyyyMMdd')
					ELSE FORMAT(cont.Status_Changed_to_Enrollment_Complete__c, 'yyyyMMdd')
					END AS [Date Enrollment Complete]
			FROM [Contact] cont
			INNER JOIN dbo.Account acct ON acct.Id=cont.Client__c
			INNER JOIN dbo.Prescribed_Drug__c pres ON pres.Contact__c=cont.Id
			INNER JOIN dbo.Drug__c drug ON drug.Id=pres.Drug__c
			WHERE cont.Tertiary_ID__c IS NOT NULL --** Member must have Tertiary ID assigned
			AND
			(
				cont.Patient_Status__c IN ('Enrollment Complete','Mbr Requests No Calls','Pending CPA Mbr','Pending CPA Pharm')
				OR 
				(
					--** $0 override patients (patients not enrolled in copay assistance)
					cont.Patient_Status__c ='Identified Ineligible'
					AND pres.Copay_Enrollment_Date__c IS NULL --** Copay enrollment date should not be set for these members.  If it is, do not send.
					AND pres.Group_ID__c IS NOT NULL --** Group ID must be set for these members.  If it is not, do not send.
				)
			)
			AND pres.Enrollment_Status__c IN ('Enrolled', 'Not Eligible') --** Member must either be enrolled in, or not eligible for, the prescribed drug
			AND ISNULL(drug.Drug_Level_Outside_Pharmacy__c, 'No') = 'No' --** Prescribed drug must not be an LDD drug

			EXCEPT

			SELECT [Id]
				,[First Name]
				,[Last Name]
				,Suffix
				,Birthdate
				,[Drug]
				,[Member ID]
				,[Tertiary ID]
				,[Copay Enrollment Date]
				,[Copay Bin]
				,[Copay ID]
				,[Group ID]
				,[Card Type]
				,[Credit Card Number]
				,[Security Code]
				,[Expiration Date]
				,[Client Name]
				,[Client Go Live]
				,[Status_Changed_to-Enrolled]
				,Status_Changed_to_Enrollment_Complete
				,[Date Enrollment Complete]
			FROM #most_current_records
		) x

		UNION

		/*************************************************************************************
		 Retrieve enrollment changes based on other Contact changes
		 *************************************************************************************/
		SELECT DISTINCT cont.Id
			, cont.FirstName AS [First Name]
			, cont.LastName AS [Last Name]
			, cont.Suffix
			, FORMAT(cont.Birthdate, 'yyyyMMdd') AS [Birthdate]
			, drug.[Name] AS [Drug]
			, cont.Member_ID__c AS [Member ID]
			, cont.Tertiary_ID__c AS [Tertiary ID]
			, FORMAT(pres.Copay_Enrollment_Date__c, 'yyyyMMdd') AS [Copay Enrollment Date]
			, pres.Copay_Bin__c AS [Copay Bin]
			, pres.Copay_ID__c AS  [Copay ID]
			, pres.Group_ID__c AS [Group ID]
			, pres.Card_Type__c AS [Card Type]
			, pres.Card_Number__c AS [Credit Card Number]
			, pres.Card_Security_Code__c AS [Security Code]
			, FORMAT(CAST(Card_Expiration_Year__c AS DATE), 'yy') + Card_Expiration_Month__c AS [Expiration Date]
			, acct.[Name] AS [Client Name]
			, FORMAT(cont.Client_Go_Live_Date__c, 'yyyyMMdd') AS [Client Go Live]
			, FORMAT(pres.[Status_Changed_to_Enrolled__c], 'yyyyMMdd') AS [Status_Changed_to-Enrolled]
			, CASE WHEN cont.Patient_Status__c = 'Identified Ineligible' THEN FORMAT(cont.Member_Eligibility_Effective_Date__c, 'yyyyMMdd')
				ELSE FORMAT(cont.Status_Changed_to_Enrollment_Complete__c, 'yyyyMMdd')
				END AS [Status_Changed_to_Enrollment_Complete]
			, CASE WHEN cont.Patient_Status__c = 'Identified Ineligible' THEN FORMAT(cont.Member_Eligibility_Effective_Date__c, 'yyyyMMdd')
				ELSE FORMAT(cont.Status_Changed_to_Enrollment_Complete__c, 'yyyyMMdd')
				END AS [Date Enrollment Complete]
		FROM [Contact] cont 
		INNER JOIN dbo.ContactHistory hist ON hist.ContactId=cont.Id
		INNER JOIN dbo.Account acct ON acct.Id=cont.Client__c
		INNER JOIN dbo.Prescribed_Drug__c pres ON pres.Contact__c=cont.Id
		INNER JOIN dbo.Drug__c drug ON drug.Id=pres.Drug__c
		WHERE
		cont.Tertiary_ID__c IS NOT NULL --** Member must have Tertiary ID assigned
		AND
		(
			( --** Member ID changes
				hist.Field IN ('Member_ID__c')
				AND hist.CreatedDate >= @prior_business_day
			)
			OR
			( --** Pass Thru Tertiary changes
				hist.Field IN ('Pass_Thru_Tertiary__c')
				AND hist.CreatedDate >= @prior_business_day
			)
			OR
			( --** Temporary Pass Thru changes
				hist.Field IN ('Temporary_Pass_Thru__c')
				AND hist.CreatedDate >= @prior_business_day
			)
		)

		UNION

		/**************************************************************************************
		 Retrieve enrollment changes based on other Prescribed Drug changes
		 **************************************************************************************/
		SELECT DISTINCT cont.Id
			, cont.FirstName AS [First Name]
			, cont.LastName AS [Last Name]
			, cont.Suffix
			, FORMAT(cont.Birthdate, 'yyyyMMdd') AS [Birthdate]
			, drug.[Name] AS [Drug]
			, cont.Member_ID__c AS [Member ID]
			, cont.Tertiary_ID__c AS [Tertiary ID]
			, FORMAT(pres.Copay_Enrollment_Date__c, 'yyyyMMdd') AS [Copay Enrollment Date]
			, pres.Copay_Bin__c AS [Copay Bin]
			, pres.Copay_ID__c AS  [Copay ID]
			, pres.Group_ID__c AS [Group ID]
			, pres.Card_Type__c AS [Card Type]
			, pres.Card_Number__c AS [Credit Card Number]
			, pres.Card_Security_Code__c AS [Security Code]
			, FORMAT(CAST(Card_Expiration_Year__c AS DATE), 'yy') + Card_Expiration_Month__c AS [Expiration Date]
			, acct.[Name] AS [Client Name]
			, FORMAT(cont.Client_Go_Live_Date__c, 'yyyyMMdd') AS [Client Go Live]
			, FORMAT(pres.[Status_Changed_to_Enrolled__c], 'yyyyMMdd') AS [Status_Changed_to-Enrolled]
			, CASE WHEN cont.Patient_Status__c = 'Identified Ineligible' THEN FORMAT(cont.Member_Eligibility_Effective_Date__c, 'yyyyMMdd')
				ELSE FORMAT(cont.Status_Changed_to_Enrollment_Complete__c, 'yyyyMMdd')
				END AS [Status_Changed_to_Enrollment_Complete]
			, CASE WHEN cont.Patient_Status__c = 'Identified Ineligible' THEN FORMAT(cont.Member_Eligibility_Effective_Date__c, 'yyyyMMdd')
				ELSE FORMAT(cont.Status_Changed_to_Enrollment_Complete__c, 'yyyyMMdd')
				END AS [Date Enrollment Complete]
		FROM [Contact] cont 
		INNER JOIN dbo.Account acct ON acct.Id=cont.Client__c
		INNER JOIN dbo.Prescribed_Drug__c pres ON pres.Contact__c=cont.Id
		INNER JOIN dbo.Drug__c drug ON drug.Id=pres.Drug__c
		INNER JOIN dbo.Prescribed_Drug__History hist ON hist.ParentId = pres.Id
		WHERE cont.Tertiary_ID__c IS NOT NULL --** Member must have Tertiary ID assigned
		AND
		(
			( --** Card Number changes
				hist.Field IN ('Card_Number__c')
				AND hist.CreatedDate >= @prior_business_day
			)
			OR
			( --** Card Expiration Month changes
				hist.Field IN ('Card_Expiration_Month__c')
				AND hist.CreatedDate >= @prior_business_day
			)
			OR
			( --** Card Expiration Year changes
				hist.Field IN ('Card_Expiration_Year__c')
				AND hist.CreatedDate >= @prior_business_day
			)
		)
	) target_enrollments



	/*************************************************************************************
	 Drop the temp table
	*************************************************************************************/
	DROP TABLE #most_current_records


END

GO



DROP PROCEDURE  IF  EXISTS [accredo].[Get_Staged_Enrollment_Data]
GO
-- =============================================
-- Author:		Tanessa Richardson
-- Create date: 12/28/2021
-- Description:	Selects data from staging table for inclusion on Accredo Enrollment file
-- =============================================
CREATE PROCEDURE [accredo].[Get_Staged_Enrollment_Data]
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Id],
		[First Name],
		[Last Name],
		Suffix,
		[Birthdate],
		[Drug],
		[Member ID],
		[Tertiary ID],
		[Copay Enrollment Date],
		[Copay Bin],
		[Copay ID],
		[Group ID],
		[Card Type],
		[Credit Card Number],
		[Security Code],
		[Expiration Date],
		[Client Name],
		[Client Go Live],
		[Status_Changed_to-Enrolled],
		[Status_Changed_to_Enrollment_Complete],
		[Date Enrollment Complete]
	FROM accredo.Enrollments_Stage


END

GO


DROP PROCEDURE IF EXISTS [accredo].[Update_Contacts_Included_on_Enrollment_File]
GO

-- =============================================
-- Author:		Tanessa Richardson
-- Create date: 12/28/2021
-- Description:	Sets the Tertiary_Sent_to_Accredo__c flag for each distinct contact included on current day Accredo Enrollment file
-- =============================================
-- VO 10/17/2022 DTE-666:	Replaced all SF_Refresh references with SF_Mirror.
-- =============================================
CREATE PROCEDURE [accredo].[Update_Contacts_Included_on_Enrollment_File]
AS
BEGIN

	SET NOCOUNT ON;

	/*****************************************************
	 Truncate the DBAmp staging table
	*****************************************************/
	TRUNCATE TABLE Contact_UPDATE_AccredoEnrollment

	/*****************************************************
	 Populate DBAmp staging table
	*****************************************************/
	INSERT INTO Contact_UPDATE_AccredoEnrollment
	(
		Id,
		Tertiary_Sent_to_Accredo__c
	)
	SELECT DISTINCT
		Id,
		1 AS [Tertiary_Sent_to_Accredo__c]
	FROM accredo.Enrollments_Stage

	/*****************************************************
	 Execute sync to SF via DBAmp
	*****************************************************/
	EXEC SF_TableLoader 'UPDATE', 'PRIME_PORTAL', 'Contact_UPDATE_AccredoEnrollment'

	/*****************************************************
	 Refresh local Contact data
	*****************************************************/
	EXEC SF_Mirror 'PRIME_PORTAL', 'Contact'

	/***********************************************************************************************
	 Raise an error if we get to this point and there are failed records in the staging result table
	************************************************************************************************/
	IF (SELECT COUNT(*) FROM Contact_UPDATE_AccredoEnrollment_Result WHERE LOWER(Error) <> 'operation successful.') > 0
		RAISERROR('Errors occurred during SalesForce Contact updates. Please check the Contact_UPDATE_AccredoEnrollment_Result table.', 11, 1)

END
GO




DROP PROCEDURE  IF  EXISTS [accredo].[Stage_Unenrollment_Data]
GO

-- =============================================
-- Author:		Senghor Etienne
-- Create date: 10/26/2021
-- Description:	Stored Procedure to grab the declined enrollments for Accredo
-- =============================================
-- TYR 20211215:	Changed Cardholder_ID__c to Member_ID__c.
--					Replaced hard-coded NULL for [Status_Changed_to-Enrolled] and [Date Enrollment Complete] aliases with appropriate object fields.
--					Formatted dates as strings in MM/dd/yyyy format.
--					Removed join to Account object as no fields from that object are in use for this data set
--					Removed order by statement
-- TYR 20211217:	Added additional business logic for Member ID defined by Chris P.
-- TYR 20220107:	JMM and I reviewed the full set of business requirements (including all new logic introduced since 12/29/2021)
--					The query has been updated to reflect the full set of business requirements as they stand as of today
-- =============================================
CREATE PROCEDURE [accredo].[Stage_Unenrollment_Data]
	
AS
BEGIN
	SET NOCOUNT ON;


	/*****************************************************/
	/* Truncate the staging table */
	/*****************************************************/
	TRUNCATE TABLE [accredo].[Unenrollments_Stage]

	/*****************************************************/
	/* Populate staging table */
	/*****************************************************/
	INSERT INTO [accredo].[Unenrollments_Stage]
			   ([Id]
			   ,[First Name]
			   ,[Last Name]
			   ,[Suffix]
			   ,[Birthdate]
			   ,[Drug]
			   ,[Member ID]
			   ,[Tertiary ID]
			   ,[Status_Changed_to_Unenrollment_Complete])
	SELECT cont.Id
		, cont.FirstName AS [First Name]
		, cont.LastName AS [Last Name]
		, cont.Suffix
		, FORMAT(cont.Birthdate, 'yyyyMMdd') AS [Birthdate]
		, drug.[Name] AS [Drug]
		, CASE
			WHEN hist.Field = 'Patient_Status__c' THEN cont.Member_ID__c --** If record included because of change in Patient Status, the current Member ID value from the Contact record is sent
			WHEN hist.Field = 'Member_ID__c' THEN hist.OldValue ELSE NULL --** If record included becasue of change in Member ID, the old Member ID value from ContactHistory is sent
		  END AS [Member ID]
		, cont.Tertiary_ID__c AS [Tertiary ID]
		, FORMAT(hist.CreatedDate, 'yyyyMMdd') AS [Status_Changed_to_Unenrollment_Complete]
	FROM [Contact] cont 
	INNER JOIN dbo.ContactHistory hist ON hist.ContactId=cont.Id
	INNER JOIN dbo.Prescribed_Drug__c pres ON pres.Contact__c=cont.Id
	INNER JOIN dbo.Drug__c drug ON drug.Id=pres.Drug__c
	WHERE cont.Tertiary_ID__c IS NOT NULL --** Member must have Tertiary ID assigned
	AND ISNULL(cont.Tertiary_Sent_to_Accredo__c, 0) = 1	--** Tertiary must have already been sent to Accredo.
															--** This is temporary logic due to the HCSC non-happy path clients.
															--** At some point, prior business day logic will be added to the Patient Status filters below.
	AND
	(
		(   
			--** Patient Status changes to Excluded
			cont.Patient_Status__c='Excluded'
			AND hist.Field='Patient_Status__c' and hist.NewValue='Excluded'
			AND hist.CreatedDate >=(SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
									WHEN 1 THEN -2 
									WHEN 2 THEN -3 
									ELSE -1 
									END, DATEDIFF(DAY, 0, GETDATE()))
								  ) 
			AND hist.OldValue IN ('Enrollment Complete', 'Inactive', 'Declined Enrollment', 'Identified Ineligible')
		)
		OR
		(
			--** Member ID changes
			hist.Field = 'Member_ID__c'
			AND hist.CreatedDate >=(SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
									WHEN 1 THEN -2 
									WHEN 2 THEN -3 
									ELSE -1 
									END, DATEDIFF(DAY, 0, GETDATE()))
								  )
			AND hist.OldValue IS NOT NULL
		)
		OR
		(	--** Patient status changes to Inactive, member no longer on drug, and member not on any other active drugs
			cont.Patient_Status__c='Inactive'
			AND hist.Field='Patient_Status__c' and hist.NewValue='Inactive'
			AND hist.CreatedDate >=(SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
									WHEN 1 THEN -2 
									WHEN 2 THEN -3 
									ELSE -1 
									END, DATEDIFF(DAY, 0, GETDATE()))
								  ) 
			AND hist.OldValue in ('Enrollment Complete', 'Identified Ineligible')
			AND pres.Enrollment_Status__c = 'No Longer on Drug'
			AND (select count(Id) from dbo.Prescribed_Drug__c where Contact__c = cont.Id and Enrollment_Status__c in ('Enrolled', 'Not Eligible')) = 0
		)
	)


END

GO


DROP PROCEDURE IF EXISTS [accredo].[Get_Staged_Unenrollment_Data]
GO

-- =============================================
-- Author:		Tanessa Richardson
-- Create date: 12/28/2021
-- Description:	Selects data from staging table for inclusion on Accredo Unenrollment file
-- =============================================
CREATE PROCEDURE [accredo].[Get_Staged_Unenrollment_Data]
	
AS
BEGIN
	SET NOCOUNT ON;

SELECT [Id]
      ,[First Name]
      ,[Last Name]
      ,[Suffix]
      ,[Birthdate]
      ,[Drug]
      ,[Member ID]
      ,[Tertiary ID]
      ,[Status_Changed_to_Unenrollment_Complete]
  FROM [accredo].[Unenrollments_Stage]

END

GO

DROP PROCEDURE IF EXISTS [accredo].[Get_Unenrollment_Data]
GO

-- =============================================
-- Author:		Senghor Etienne
-- Create date: 10/26/2021
-- Description:	Stored Procedure to grab the declined enrollments for Accredo
-- =============================================
-- TYR 20211215:	Changed Cardholder_ID__c to Member_ID__c.
--					Replaced hard-coded NULL for [Status_Changed_to-Enrolled] and [Date Enrollment Complete] aliases with appropriate object fields.
--					Formatted dates as strings in MM/dd/yyyy format.
--					Removed join to Account object as no fields from that object are in use for this data set
--					Removed order by statement
-- TYR 20211217:	Added additional business logic for Member ID defined by Chris P.
-- TYR 20220107:	JMM and I reviewed the full set of business requirements (including all new logic introduced since 12/29/2021)
--					The query has been updated to reflect the full set of business requirements as they stand as of today
-- =============================================
CREATE PROCEDURE [accredo].[Get_Unenrollment_Data]
	
AS
BEGIN
	SET NOCOUNT ON;


	SELECT cont.FirstName AS [First Name]
		, cont.LastName AS [Last Name]
		, cont.Suffix
		, FORMAT(cont.Birthdate, 'yyyyMMdd') AS [Birthdate]
		, drug.[Name] AS [Drug]
		, CASE
			WHEN hist.Field = 'Patient_Status__c' THEN cont.Member_ID__c --** If record included because of change in Patient Status, the current Member ID value from the Contact record is sent
			WHEN hist.Field = 'Member_ID__c' THEN hist.OldValue ELSE NULL --** If record included becasue of change in Member ID, the old Member ID value from ContactHistory is sent
		  END AS [Member ID]
		, cont.Tertiary_ID__c AS [Tertiary ID]
		, FORMAT(hist.CreatedDate, 'yyyyMMdd') AS [Status_Changed_to_Unenrollment_Complete]
		, cont.Id
		, cont.Patient_Status__c
	FROM [Contact] cont 
	INNER JOIN dbo.ContactHistory hist ON hist.ContactId=cont.Id
	INNER JOIN dbo.Prescribed_Drug__c pres ON pres.Contact__c=cont.Id
	INNER JOIN dbo.Drug__c drug ON drug.Id=pres.Drug__c
	WHERE cont.Tertiary_ID__c IS NOT NULL --** Member must have Tertiary ID assigned
	AND ISNULL(cont.Tertiary_Sent_to_Accredo__c, 0) = 1	--** Tertiary must have already been sent to Accredo.
															--** This is temporary logic due to the HCSC non-happy path clients.
															--** At some point, prior business day logic will be added to the Patient Status filters below.
	AND
	(
		( --** Patient Status changes to Excluded
			cont.Patient_Status__c='Excluded'
			AND hist.Field='Patient_Status__c' and hist.NewValue='Excluded'
			AND hist.CreatedDate >=(SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
									WHEN 1 THEN -2 
									WHEN 2 THEN -3 
									ELSE -1 
									END, DATEDIFF(DAY, 0, GETDATE()))
								  ) 
			AND hist.OldValue IN ('Enrollment Complete', 'Inactive', 'Declined Enrollment', 'Identified Ineligible')
		)
		OR
		(
			--** Member ID changes
			hist.Field = 'Member_ID__c'
			AND hist.CreatedDate >=(SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
									WHEN 1 THEN -2 
									WHEN 2 THEN -3 
									ELSE -1 
									END, DATEDIFF(DAY, 0, GETDATE()))
								  )
			AND hist.OldValue IS NOT NULL
		)
		OR
		(	--** Patient status changes to Inactive, member no longer on drug, and member not on any other active drugs
			cont.Patient_Status__c='Inactive'
			AND hist.Field='Patient_Status__c' and hist.NewValue='Inactive'
			AND hist.CreatedDate >=(SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
									WHEN 1 THEN -2 
									WHEN 2 THEN -3 
									ELSE -1 
									END, DATEDIFF(DAY, 0, GETDATE()))
								  ) 
			AND hist.OldValue in ('Enrollment Complete', 'Identified Ineligible')
			AND pres.Enrollment_Status__c = 'No Longer on Drug'
			AND (select count(Id) from dbo.Prescribed_Drug__c where Contact__c = cont.Id and Enrollment_Status__c in ('Enrolled', 'Not Eligible')) = 0
		)
	)


END

GO


DROP PROCEDURE  IF  EXISTS [accredo].[Stage_Declined_Enrollment_Data]
GO

-- =============================================
-- Author:		Senghor Etienne
-- Create date: 10/26/2021
-- Description:	Stored Procedure to grab the declined enrollments for Accredo
-- =============================================
-- TYR 20211215:	Changed Cardholder_ID__c to Member_ID__c.
--					In hist.OldValue filter list, changed 'Non-Responsive' (invalid value from file spec) to 'Non-Responsive Member' (actual SF value).
--					Formatted dates as strings in yyyyMMdd format.
--					Changed join to Account table to match on Client__c.  Match was previously on AccountId, which ties to the member account, not the client.  
-- =============================================
CREATE PROCEDURE [accredo].[Stage_Declined_Enrollment_Data]
	
AS
BEGIN
	SET NOCOUNT ON;

	/*****************************************************/
	/* Truncate the staging table */
	/*****************************************************/
	TRUNCATE TABLE [accredo].[Declined_Enrollments_Stage]

	/*************************************************************************************
	 Populate staging table. Retrieve declined enrollments based on Contact Patient Status
	**************************************************************************************/
	INSERT INTO [accredo].[Declined_Enrollments_Stage]
			   ([Id]
			   ,[First Name]
			   ,[Last Name]
			   ,[Suffix]
			   ,[Birthdate]
			   ,[Drug]
			   ,[Member ID]
			   ,[Client Name]
			   ,[Client Go Live])
	SELECT cont.Id
		, cont.FirstName AS [First Name]
		, cont.LastName AS [Last Name]
		, cont.Suffix
		, FORMAT(cont.Birthdate, 'yyyyMMdd') AS [Birthdate]
		, drug.[Name] AS [Drug]
		, cont.Member_ID__c AS [Member ID]
		, acct.Name AS [Client Name]
		, FORMAT(cont.Client_Go_Live_Date__c, 'yyyyMMdd') AS [Client Go Live] 
	FROM [Contact] cont 
	INNER JOIN dbo.ContactHistory hist ON hist.ContactId=cont.Id
	INNER JOIN dbo.Account acct ON acct.Id=cont.Client__c
	INNER JOIN dbo.Prescribed_Drug__c pres ON pres.Contact__c=cont.Id
	INNER JOIN dbo.Drug__c drug ON drug.Id=pres.Drug__c
	WHERE cont.Patient_Status__c='Declined Enrollment'
	AND hist.Field='Patient_Status__c' AND hist.NewValue='Declined Enrollment'
	AND hist.CreatedDate >=(SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
							WHEN 1 THEN -2 
							WHEN 2 THEN -3 
							ELSE -1 
							END, DATEDIFF(DAY, 0, GETDATE()))
							) 
	AND hist.OldValue IN ('Eligible', 'Enrollment Complete', 'Inactive', 'Non-Responsive Member', 'Pending CPA','Excluded','Identified Ineligible')



END

GO


DROP PROCEDURE IF EXISTS  [accredo].[Get_Staged_Declined_Enrollment_Data]
GO
-- =============================================
-- Author:		Tanessa Richardson
-- Create date: 12/28/2021
-- Description:	Selects data from staging table for inclusion on Accredo Declined Enrollment file
-- =============================================
CREATE PROCEDURE [accredo].[Get_Staged_Declined_Enrollment_Data]
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Id]
		  ,[First Name]
		  ,[Last Name]
		  ,[Suffix]
		  ,[Birthdate]
		  ,[Drug]
		  ,[Member ID]
		  ,[Client Name]
		  ,[Client Go Live]
	  FROM [accredo].[Declined_Enrollments_Stage]

END

GO



DROP PROCEDURE IF EXISTS   [accredo].[Get_Declined_Enrollment_Data]
GO
-- =============================================
-- Author:		Senghor Etienne
-- Create date: 10/26/2021
-- Description:	Stored Procedure to grab the declined enrollments for Accredo
-- =============================================
-- TYR 20211215:	Changed Cardholder_ID__c to Member_ID__c.
--					In hist.OldValue filter list, changed 'Non-Responsive' (invalid value from file spec) to 'Non-Responsive Member' (actual SF value).
--					Formatted dates as strings in yyyyMMdd format.
--					Changed join to Account table to match on Client__c.  Match was previously on AccountId, which ties to the member account, not the client.  
-- =============================================
CREATE PROCEDURE [accredo].[Get_Declined_Enrollment_Data]
	
AS
BEGIN
	SET NOCOUNT ON;


	SELECT cont.FirstName AS [First Name]
		, cont.LastName AS [Last Name]
		, cont.Suffix
		, FORMAT(cont.Birthdate, 'yyyyMMdd') AS [Birthdate]
		, drug.[Name] AS [Drug]
		, cont.Member_ID__c AS [Member ID]
		, acct.Name AS [Client Name]
		, FORMAT(cont.Client_Go_Live_Date__c, 'yyyyMMdd') AS [Client Go Live] 
	FROM [Contact] cont 
	INNER JOIN dbo.ContactHistory hist ON hist.ContactId=cont.Id
	INNER JOIN dbo.Account acct ON acct.Id=cont.Client__c
	INNER JOIN dbo.Prescribed_Drug__c pres ON pres.Contact__c=cont.Id
	INNER JOIN dbo.Drug__c drug ON drug.Id=pres.Drug__c
	WHERE cont.Patient_Status__c='Declined Enrollment'
	AND hist.Field='Patient_Status__c' AND hist.NewValue='Declined Enrollment'
	AND hist.CreatedDate >=(SELECT  DATEADD(DAY, CASE (DATEPART(WEEKDAY, GETDATE()) + @@DATEFIRST) % 7   --evaluate prior business day logic
							WHEN 1 THEN -2 
							WHEN 2 THEN -3 
							ELSE -1 
							END, DATEDIFF(DAY, 0, GETDATE()))
							) 
	AND hist.OldValue IN ('Eligible', 'Enrollment Complete', 'Inactive', 'Non-Responsive Member', 'Pending CPA','Excluded','Identified Ineligible')



END

GO


